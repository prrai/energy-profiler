*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********
-----------------------------------------------
		   L1 Cache
-----------------------------------------------
L1-dcache-stores		0
L1-dcache-loads		218,659,080,455
Total L1 HITS:		218659080455

-----------------------------------------------
		   L2 Cache
-----------------------------------------------
L2 ACCESS: 		7,310,696,391
Total L2 HITS:		7310696391

-----------------------------------------------
		   LLC Cache
-----------------------------------------------
LLC-loads		5,164,353,559
LLC-stores		22,506,204
LLC-prefetches		1,044,512,707
Total LLC HITS:		6231372470

-----------------------------------------------
		   Look Ahead Buffer
-----------------------------------------------
dTLB-loads		218,463,615,654
iTLB-loads		2,972,493
dTLB-stores		76,141,760,084
Total TLB HITS:		294608348231

-----------------------------------------------
		   DRAM
-----------------------------------------------
DRAM ACCESS: 		46,668
Total DRAM HITS:	46668
*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********


*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********
------------------------------------------------------
		   L1 Cache
------------------------------------------------------
Retired loads L1 cache hits as data source:	299691631
------------------------------------------------------
		   L2 Cache
------------------------------------------------------
Retired loads L2 cache hits as data source:	314792
------------------------------------------------------
		   LLC Cache
------------------------------------------------------
Retired loads LLC cache hits as data source:	345317
------------------------------------------------------
		   Look Ahead Buffer
------------------------------------------------------
Retired loads missed L1 but hit Look Ahead buffer:	9767954
*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********


************** ENERGY CONSUMPTION *************
Model: For each level in the memory hierarchy: 
Energy = (Em * Total number of access), where 
Em => Energy utilization per instruction (EPI).
We computed number of access in the table above. 
NOTE: FOR THE CALCULATION BELOW, we have used EPI for Intel Xeon from the paper: 
http://www.eecs.harvard.edu/~shao/papers/shao2013-islped.pdf
For exact values on a different processor, EPI values need to be modified in code: memUsage.py


Total energy due to L1 Cache access: EPI(L1) * L1-access = 192.4199908 J
Total energy due to L2 Cache access: EPI(L2) * L2-access = 56.4385761385 J
Total energy due to LLC Cache access: EPI(LLC) * LLC-access = [EPI_LLC * 10^-9 * 6231372470] J
Total energy due to Look Ahead Buffer access: EPI(LB) * LB-access = [EPI_LB * 10^-9 * 294608348231] J
Total energy due to DRAM Cache access: EPI(DRAM) * DRAM-access = 0.00266660952 J


************** ENERGY CONSUMPTION *************
