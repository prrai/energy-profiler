*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********
-----------------------------------------------
		   L1 Cache
-----------------------------------------------
L1-dcache-stores		6,003,527
L1-dcache-loads		198,188,268,487
Total L1 HITS:		198194272014

-----------------------------------------------
		   L2 Cache
-----------------------------------------------
L2 ACCESS: 		7,326,917,528
Total L2 HITS:		7326917528

-----------------------------------------------
		   LLC Cache
-----------------------------------------------
LLC-loads		5,390,498,562
LLC-stores		21,578,767
LLC-prefetches		55,361,845
Total LLC HITS:		5467439174

-----------------------------------------------
		   Look Ahead Buffer
-----------------------------------------------
dTLB-loads		209,727,305,521
iTLB-loads		3,021,998
dTLB-stores		73,330,852,790
Total TLB HITS:		283061180309

-----------------------------------------------
		   DRAM
-----------------------------------------------
DRAM ACCESS: 		31,993
Total DRAM HITS:	31993
*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********


*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********
------------------------------------------------------
		   L1 Cache
------------------------------------------------------
Retired loads L1 cache hits as data source:	10951741983
------------------------------------------------------
		   L2 Cache
------------------------------------------------------
Retired loads L2 cache hits as data source:	41431032
------------------------------------------------------
		   LLC Cache
------------------------------------------------------
Retired loads LLC cache hits as data source:	33961019
------------------------------------------------------
		   Look Ahead Buffer
------------------------------------------------------
Retired loads missed L1 but hit Look Ahead buffer:	818631667
*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********


************** ENERGY CONSUMPTION *************
Model: For each level in the memory hierarchy: 
Energy = (Em * Total number of access), where 
Em => Energy utilization per instruction (EPI).
We computed number of access in the table above. 
NOTE: FOR THE CALCULATION BELOW, we have used EPI for Intel Xeon from the paper: 
http://www.eecs.harvard.edu/~shao/papers/shao2013-islped.pdf
For exact values on a different processor, EPI values need to be modified in code: memUsage.py


Total energy due to L1 Cache access: EPI(L1) * L1-access = 174.410959372 J
Total energy due to L2 Cache access: EPI(L2) * L2-access = 56.5638033162 J
Total energy due to LLC Cache access: EPI(LLC) * LLC-access = [EPI_LLC * 10^-9 * 5467439174] J
Total energy due to Look Ahead Buffer access: EPI(LB) * LB-access = [EPI_LB * 10^-9 * 283061180309] J
Total energy due to DRAM Cache access: EPI(DRAM) * DRAM-access = 0.00182808002 J


************** ENERGY CONSUMPTION *************
