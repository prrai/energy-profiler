#!/bin/bash/

args=("$@")

timeStamp=$(date +%s)

sudo /usr/src/linux-4.7.1/tools/perf/perf stat -e dTLB-stores,dTLB-loads,iTLB-loads,L1-dcache-loads,L1-dcache-stores,L1-dcache-prefetches,L1-icache-loads,L1-icache-prefetches,cpu/event=0x24,umask=0xff/u,dTLB-prefetches,LLC-loads,LLC-stores,LLC-prefetches,cpu/event=0xd3,umask=0x01/u,cpu/event=0xd1,umask=0x00/u,cpu/event=0xd1,umask=0x02/u,cpu/event=0xd1,umask=0x04/u,cpu/event=0xd1,umask=0x40/u,cpu/event=0xd1,umask=0x01/u  ${args[0]} "${@:2}" 2>mem_data_out_$timeStamp.txt

echo "Dumped metrics to file: mem_data_out_$timeStamp.txt"

echo "Now parse metrics to generate report.."
python memUsage.py mem_data_out_$timeStamp.txt >OUT_mem_data_out_$timeStamp.txt

echo "Parsing complete, generate profiled memory report as: OUT_mem_data_out_$timeStamp.txt"
