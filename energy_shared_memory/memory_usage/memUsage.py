import sys
import re

EPI_L1 = 0.88
EPI_L2 = 7.72
EPI_LLC = -1
EPI_LB = -1
EPI_DRAM = (52.14 + 62.14) / 2


def extract_entries():
    res = []
    with open(sys.argv[1]) as ip_data:
        ip_data = ip_data.read()
        for item in ip_data.split('\n'):
            if "not supported" not in item:
                stuff = item.split()
            else:
                stuff = item.split('>')
            stuff = filter(None, stuff)
            stuff = [x for x in stuff if not x.startswith(' Performance counter')]
            if len(stuff) > 0:
                res.append(stuff)
    return res


def compute():
    res = extract_entries()
    L1_data = {}
    L2_data = {}
    LLC_data = {}
    TLB_data = {}
    DRAM_data = {}
    L1_access = 0
    L2_access = 0
    LLC_access = 0
    TLB_access = 0
    DRAM_access = 0
    RET_L1 = 0
    RET_L2 = 0
    RET_LLC = 0
    RET_LFB = 0
    RET_L1_XEON = 0
    for item in res:
        if len(item) > 1 and "not supported" not in item[0]:
            if 'TLB' in item[1]:
                TLB_data[item[1]] = item[0]
                TLB_access += int(item[0].replace(',', ''))
            if 'L1-' in item[1]:
                L1_data[item[1]] = item[0]
                L1_access += int(item[0].replace(',', ''))
            if 'LLC' in item[1]:
                LLC_data[item[1]] = item[0]
                LLC_access += int(item[0].replace(',', ''))
            if 'cpu/event=0x24' in item[1]:
                L2_data[item[1]] = item[0]
                L2_access += int(item[0].replace(',', ''))
            if 'cpu/event=0xd3' in item[1]:
                DRAM_data[item[1]] = item[0]
                DRAM_access += int(item[0].replace(',', ''))
            if 'cpu/event=0xd1,umask=0x00/u' in item[1]:
                RET_L1 += int(item[0].replace(',', ''))
            if 'cpu/event=0xd1,umask=0x01/u' in item[1]:
                RET_L1_XEON += int(item[0].replace(',', ''))
            if 'cpu/event=0xd1,umask=0x02/u' in item[1]:
                RET_L2 += int(item[0].replace(',', ''))
            if 'cpu/event=0xd1,umask=0x04/u' in item[1]:
                RET_LLC += int(item[0].replace(',', ''))
            if 'cpu/event=0xd1,umask=0x40/u' in item[1]:
                RET_LFB += int(item[0].replace(',', ''))

    if RET_L1 == 0:
        RET_L1 = RET_L1_XEON
    print "*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********"
    print "-----------------------------------------------"
    print "\t\t   L1 Cache"
    print "-----------------------------------------------"
    for key in L1_data:
        print "{0}\t\t{1}".format(key, L1_data[key])
    print "Total L1 HITS:\t\t{0}".format(L1_access)
    print "\n-----------------------------------------------"
    print "\t\t   L2 Cache"
    print "-----------------------------------------------"
    for key in L2_data:
        print "L2 ACCESS: \t\t{1}".format(key, L2_data[key])
    print "Total L2 HITS:\t\t{0}".format(L2_access)
    print "\n-----------------------------------------------"
    print "\t\t   LLC Cache"
    print "-----------------------------------------------"
    for key in LLC_data:
        print "{0}\t\t{1}".format(key, LLC_data[key])
    print "Total LLC HITS:\t\t{0}".format(LLC_access)
    print "\n-----------------------------------------------"
    print "\t\t   Look Ahead Buffer"
    print "-----------------------------------------------"
    for key in TLB_data:
        print "{0}\t\t{1}".format(key, TLB_data[key])
    print "Total TLB HITS:\t\t{0}".format(TLB_access)
    print "\n-----------------------------------------------"
    print "\t\t   DRAM"
    print "-----------------------------------------------"
    for key in DRAM_data:
        print "DRAM ACCESS: \t\t{0}".format(DRAM_data[key])
    print "Total DRAM HITS:\t{0}".format(DRAM_access)
    print "*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********"

    print "\n\n*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********"
    print "------------------------------------------------------"
    print "\t\t   L1 Cache"
    print "------------------------------------------------------"
    print "Retired loads L1 cache hits as data source:\t" + str(RET_L1)
    print "------------------------------------------------------"
    print "\t\t   L2 Cache"
    print "------------------------------------------------------"
    print "Retired loads L2 cache hits as data source:\t" + str(RET_L2)
    print "------------------------------------------------------"
    print "\t\t   LLC Cache"
    print "------------------------------------------------------"
    print "Retired loads LLC cache hits as data source:\t" + str(RET_LLC)
    print "------------------------------------------------------"
    print "\t\t   Look Ahead Buffer"
    print "------------------------------------------------------"
    print "Retired loads missed L1 but hit Look Ahead buffer:\t" + str(RET_LFB)
    print "*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********"

    print "\n\n************** ENERGY CONSUMPTION *************"
    print "Model: For each level in the memory hierarchy: \nEnergy = (Em * Total number of access), where \nEm => " \
          "Energy utilization per instruction (EPI)."
    print "We computed number of access in the table above. "
    print "NOTE: FOR THE CALCULATION BELOW, we have used EPI for Intel Xeon from the paper: \n" \
          "http://www.eecs.harvard.edu/~shao/papers/shao2013-islped.pdf"
    print "For exact values on a different processor, EPI values need to be modified in code: memUsage.py"

    print "\n"
    print "Total energy due to L1 Cache access: EPI(L1) * L1-access = {0} J".format("[EPI_L1 * 10^-9 * {0}]".format(
        L1_access) if EPI_L1 is -1 else EPI_L1 * L1_access * 10 ** -9)
    print "Total energy due to L2 Cache access: EPI(L2) * L2-access = {0} J".format("[EPI_L2 * 10^-9 * {0}]".format(
        L2_access) if EPI_L2 is -1 else EPI_L2 * L2_access * 10 ** -9)
    print "Total energy due to LLC Cache access: EPI(LLC) * LLC-access = {0} J".format("[EPI_LLC * 10^-9 * {0}]".format(
        LLC_access) if EPI_LLC is -1 else EPI_LLC * LLC_access * 10 ** -9)
    print "Total energy due to Look Ahead Buffer access: EPI(LB) * LB-access = {0} J".format("[EPI_LB * 10^-9 * {0}]"
                                                                                             .format(TLB_access)
                                                                                             if EPI_LB is -1 else
                                                                                             EPI_LB * TLB_access *
                                                                                             10 ** -9)
    print "Total energy due to DRAM Cache access: EPI(DRAM) * DRAM-access = {0} J".format(
        "[EPI_DRAM * 10^-9 * {0}]".format(
            DRAM_access) if EPI_DRAM is -1 else EPI_DRAM * DRAM_access * 10 ** -9)
    print "\n\n************** ENERGY CONSUMPTION *************"


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage: python memUsage.py <path_to_perf_log_file> \n Example: python memUsage.py mem_data_out.txt"
        sys.exit(-1)
    compute()
