
 Performance counter stats for './a.out 11 3':

    10,347,640,608 dTLB-stores                                                  [53.54%]
    25,969,871,909 dTLB-loads                                                   [53.46%]
         6,453,087 iTLB-loads                                                   [53.38%]
    16,780,795,944 L1-dcache-loads                                              [53.42%]
                 0 L1-dcache-stores                                             [53.32%]
   <not supported> L1-dcache-prefetches    
   <not supported> L1-icache-loads         
   <not supported> L1-icache-prefetches    
     1,041,878,117 cpu/event=0x24,umask=0xff/u                                    [53.25%]
   <not supported> dTLB-prefetches         
        66,717,023 LLC-loads                                                    [53.26%]
        10,163,192 LLC-stores                                                   [53.30%]
       185,978,231 LLC-prefetches                                               [13.45%]
            13,446 cpu/event=0xd3,umask=0x01/u                                    [20.26%]
                 0 cpu/event=0xd1,umask=0x00/u                                    [26.95%]
       110,430,528 cpu/event=0xd1,umask=0x02/u                                    [33.65%]
         3,012,853 cpu/event=0xd1,umask=0x04/u                                    [40.31%]
       352,417,705 cpu/event=0xd1,umask=0x40/u                                    [46.97%]
     8,806,181,906 cpu/event=0xd1,umask=0x01/u                                    [53.61%]

       2.484546836 seconds time elapsed

