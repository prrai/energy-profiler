*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********
-----------------------------------------------
		   L1 Cache
-----------------------------------------------
L1-dcache-stores		77,575,869
L1-dcache-loads		298,801,276
Total L1 HITS:		376377145

-----------------------------------------------
		   L2 Cache
-----------------------------------------------
L2 ACCESS: 		3,328,487
Total L2 HITS:		3328487

-----------------------------------------------
		   LLC Cache
-----------------------------------------------
LLC-loads		1,478,199
LLC-stores		435,282
LLC-prefetches		1,799,599
Total LLC HITS:		3713080

-----------------------------------------------
		   Look Ahead Buffer
-----------------------------------------------
dTLB-loads		140,650,743
iTLB-loads		19,771
dTLB-stores		60,499,153
Total TLB HITS:		201169667

-----------------------------------------------
		   DRAM
-----------------------------------------------
DRAM ACCESS: 		1,089
Total DRAM HITS:	1089
*********** NUMBER OF MEMORY ACCESS (INSTRUCTIONS) ***********


*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********
------------------------------------------------------
		   L1 Cache
------------------------------------------------------
Retired loads L1 cache hits as data source:	0
------------------------------------------------------
		   L2 Cache
------------------------------------------------------
Retired loads L2 cache hits as data source:	36933
------------------------------------------------------
		   LLC Cache
------------------------------------------------------
Retired loads LLC cache hits as data source:	345
------------------------------------------------------
		   Look Ahead Buffer
------------------------------------------------------
Retired loads missed L1 but hit Look Ahead buffer:	0
*********** NUMBER OF RETIRED MEMORY ACCESS (Useful work) ***********


************** ENERGY CONSUMPTION *************
Model: For each level in the memory hierarchy: 
Energy = (Em * Total number of access), where 
Em => Energy utilization per instruction (EPI).
We computed number of access in the table above. 
NOTE: FOR THE CALCULATION BELOW, we have used EPI for Intel Xeon from the paper: 
http://www.eecs.harvard.edu/~shao/papers/shao2013-islped.pdf
For exact values on a different processor, EPI values need to be modified in code: memUsage.py


Total energy due to L1 Cache access: EPI(L1) * L1-access = 0.3312118876 J
Total energy due to L2 Cache access: EPI(L2) * L2-access = 0.02569591964 J
Total energy due to LLC Cache access: EPI(LLC) * LLC-access = [EPI_LLC * 10^-9 * 3713080] J
Total energy due to Look Ahead Buffer access: EPI(LB) * LB-access = [EPI_LB * 10^-9 * 201169667] J
Total energy due to DRAM Cache access: EPI(DRAM) * DRAM-access = 6.222546e-05 J


************** ENERGY CONSUMPTION *************
