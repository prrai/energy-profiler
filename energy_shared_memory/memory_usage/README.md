# Note
    We report both retirement access as well as total access made to respective memory levels.

    From: http://stackoverflow.com/questions/22368835/what-does-intel-mean-by-retired
     In performance context this is the number you should check to compute how many instructions were
     really executed (with useful output). If your context is energy saving, you may check how many instructions
     started their execution in OOO pipeline ("ISSUED" counter or "EXECUTED" counter) and compare the number
     with count of retired operation; high difference shows that CPU does a lot of useless work and uses excess power.

    Thus, the energy calculations are being made for the total ISSUED counts, while we also report the retired counts
    as a measure to check performance and scope of memory optimization for the target program.


    We define global EPI values below:
    For each memory level in the hierarchy, we define
    Em => Energy used per instruction (EPI in nJ). The values below are for Intel XEON taken via power instrumentation from
    the paper: http://www.eecs.harvard.edu/~shao/papers/shao2013-islped.pdf


#Usage
```
sudo bash ./execute.sh <program> <program-args>
```
