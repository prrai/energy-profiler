import re
import sys
import csv

K = 6 * 10**-6 

def core_input_parse(core_list, max_cores):
    samples = int(core_list[-1][0])
    
    i = 0
    core_visited_mark = {}
    core_exec_count = {}

    core_exec_count = {k:0 for k in range(max_cores)}
    cpu_util_avg = {k:0 for k in range(max_cores)}

    for cur_sample in range(1,samples+1):
        core_visited_mark = {k:False for k in range(max_cores)}

        while (i<len(core_list) and core_list[i][0] == cur_sample):
            if (not core_visited_mark[core_list[i][1]]):
                core_exec_count[core_list[i][1]] += 1
                cpu_util_avg[core_list[i][1]] += core_list[i][2]
#            else:
#                print("Dup found in sample :",cur_sample)
            core_visited_mark[core_list[i][1]] = True
            i += 1
    
# Now lets average out values in cpu_util using counts we have in core_exec_count
    for key,value in core_exec_count.iteritems():
        if (value != 0):
            cpu_util_avg[key] = cpu_util_avg[key] / value
        else:
            cpu_util_avg[key] = 0

    return core_exec_count, cpu_util_avg

def freq_input_parse(freq_dict, max_cores):
    core_freq = {k:0 for k in range(max_cores)}

    for key, value in freq_dict.iteritems():
        for item in range(max_cores):
            core_freq[item] += freq_dict[key][item]
    for item in range(max_cores):
        core_freq[item] /= len(freq_dict)
    return core_freq

def main(argv):
    if len(argv) < 5:
        print("Usage python calculate.py core_file freq_file energy_file max_freq_file cpu_requested_file")
        return
    core_file = argv[1]
    freq_file = argv[2]
    energy_file = argv[3]
    max_freq_file = argv[4]
    with open(argv[5], 'r') as inp:
        inp = inp.read()
    req = int(inp)
    core_reader = csv.reader(open(core_file, 'r'))
    freq_reader = csv.reader(open(freq_file, 'r'))

    with open(energy_file) as f:
        lines = f.readlines()
# Hardcoding read from energy file
    time = float(re.split("\t|\n| ",lines[-1])[2])
    energy = float(re.split("\t|\n| ",lines[-3])[3])


    print "Time taken by the entire process: ",time
    print "Energy consumed by the entire process from RAPL counters: ",energy
    

###
# Lets calculate freq details of each core
###
    freq_dict = {}
    for row in freq_reader:
        if row and len(row)>1:
            freq_dict.setdefault(int(row[0]), []).append(int(row[1]))
    max_cores = len(freq_dict[1])
# Return a dictionary containing the average frequencies on which the cores were run
    core_freq = freq_input_parse(freq_dict, max_cores)

###
# Lets get max freq details of each core
###
    max_freq_dict = {k:0 for k in range(max_cores)}
    with open(max_freq_file) as f:
        lines = f.readlines()
    if (len(lines) == max_cores):
        for k in range(max_cores):
            max_freq_dict[k] = int(lines[k])
    else:
        print("ERROR: Mismatch in number of cores and number of entries in max_freq file")

    avg_max_freq = sum(max_freq_dict.itervalues())/ max_cores

###
# Lets calculate core exec counts and cpu util details
###
    core_list = []
    for row in core_reader:
        if row:
            core_list.append([ float(x) for x in row ])
    samples = int(core_list[-1][0])

# Returns a dictionary containing the number of times it was executed upon 
# and also a dictionary containing cpu util%
    core_exec_count, cpu_util_avg = core_input_parse(core_list, max_cores)
    
    print "Number of samples taken :", samples
    print "Number of times from",samples,"samples the process ran on the cores : ",core_exec_count
    
# Find relative time of each core
    for key,value in core_exec_count.iteritems():
        core_exec_count[key] = float(value)/samples * time
    
    print "The relative time which the cores were busy executing the process: ",core_exec_count
    print "Average Core frequencies during the run: ",core_freq
    print "Average CPU utilizations during the run: ", cpu_util_avg
    print "Max Core frequencies: ",max_freq_dict
    print "Average Max freq : F: ",avg_max_freq


#  (t1 * cpu1 * X1^3) + (t2 * cpu2 * X2^3).....
    calculated_energy = {k:0 for k in range(max_cores)}
    for k in range(max_cores):
        calculated_energy[k] = core_exec_count[k] * (core_freq[k]**3) * (cpu_util_avg[k] * 0.01)
    sum_term = sum(calculated_energy.itervalues())

    print "\n\nCalculated energy = sum(Time * cpu_util * freq^3) * E_d"
    print "Summation term: ",calculated_energy," : ", sum_term
    print "Energy estimated by the model in Joules: ", ( K * sum_term ) / (avg_max_freq**2) / req

###
# Find the constant E_d
# rapl_energy = (k/(F^2)) * (sum_term)
###
    
    print "\nrapl_energy = (k/(F^2)) * (sum_term)"
    konstant = (energy/sum_term) * (avg_max_freq**2)
    konstant*=req
    print "K :",konstant

if __name__ == "__main__":
    main(sys.argv)


