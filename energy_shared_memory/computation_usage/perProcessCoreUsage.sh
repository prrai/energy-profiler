#!/bin/sh

rm -f freq_samples.txt
rm -f max_freq_per_core.txt
rm -f core_usage_samples.txt

sudo cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_max_freq > max_freq_per_core.txt
cpus=$(wc -l < max_freq_per_core.txt)
echo "CPUs: $cpus" >> freq_samples.txt 
pids=()
while [ $# != 0 ]; do
        pids=("${pids[@]}" "$1")
        shift
done

if [ -z "${pids[0]}" ]; then
        echo "Usage: $0 <pid1> [pid2] ..."
        exit 1
fi

for pid in "${pids[@]}"; do
        if [ ! -e /proc/$pid ]; then
                echo "Error: pid $pid doesn't exist"
                exit 1
        fi
done
sample=0
while [ true ]; do
	    sample=$((sample+1))
        for pid in "${pids[@]}"; do
               ps -p $pid -L -o pid,tid,psr,pcpu,comm | sed -n '1!p' | awk '{print "'"$sample"'",","$3,","$4}'  >> core_usage_samples.txt
			   echo "" >> core_usage_samples.txt
        done
		sudo cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq | awk '{print "'"$sample"'",","$1}' >> freq_samples.txt
		echo "" >> freq_samples.txt
        sleep 0.1
		if [ ! -e /proc/$pid ]; then
		  exit 0
		fi
done
