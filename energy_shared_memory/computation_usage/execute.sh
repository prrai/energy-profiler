#!/bin/bash

args=("$@")
modprobe msr
./AppPowerMeter ${args[0]} "${@:2}" > apppowermeter.txt &

name=`echo ${args[0]} | cut -f2 -d"/"`
pname=`pgrep $name`
echo "pid for" $name "is:" $pname

bash perProcessCoreUsage.sh $pname
python cpuRequested.py > cpu_requested.txt
python calculate.py core_usage_samples.txt freq_samples.txt apppowermeter.txt max_freq_per_core.txt cpu_requested.txt
#sudo /usr/src/linux-4.7.1/tools/perf/perf stat -p $pname -e dTLB-stores,dTLB-loads,iTLB-loads,L1-dcache-loads,L1-dcache-stores,L1-dcache-prefetches,L1-icache-loads,L1-icache-prefetches,cpu/event=0x24,umask=0xff/u,dTLB-prefetches,LLC-loads,LLC-stores,LLC-prefetches,cpu/event=0xd3,umask=0x01/u  1>prog_out.txt 2>mem_data_out.txt

