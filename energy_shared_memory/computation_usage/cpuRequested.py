import sys

with open('core_usage_samples.txt', 'r') as ip_data:
    ip_data = ip_data.read()
    ip_data = ip_data.split('\n\n')
    req = 0
    for item in ip_data:
        cur = int(len(item.split('\n')))
        req = max(req, cur)
    print req
