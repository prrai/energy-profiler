# Building mpiP
```
./configure --disable-libunwind
make
make install
```

# Example Usage
```
mpicc -g -o ping_pong ping_pong.cpp -L mpiP/lib -lmpiP -lm -lbfd -liberty
```


# After building and running program
Then run parse.py on the mpiP generated report file
