import re
import sys
import csv

power = 20      #Hardcoding to 20 KW

def time_to_hours(milli):
    hours=(milli/(1000*60*60))
    return hours

def parse_aggregate_time(table_str):
    agg_obj = [item.split() for item in table_str.splitlines()]

    send_time_list = []
    # Lets iterate for each line in the loop and check whether
    for line in agg_obj:
        if (line[0] == "Send"):
            send_time_list.append(float(line[2]))

    return send_time_list

def parse_aggregate_sent(table_str):
    agg_obj = [item.split() for item in table_str.splitlines()]

    return

def main(argv):
    global power
    mpi_log_file = argv[1]

    host_list = []

    with open(mpi_log_file) as f:
        line_list = [line.split() for line in f]

    with open(mpi_log_file) as f:
        entire_file = f.read()

    for line in line_list:
        if (" ".join(line[1:4]) == "MPI Task Assignment"):
            host_list.append(line[-1])
    print ("Host list = %s"%(host_list))

    searchObj = re.search( "Aggregate Time .*[-\s]*([^-]*)", entire_file, re.M)
    send_time_list = parse_aggregate_time(searchObj.group(1))

    print("Send time list = %s" %(send_time_list))
    searchObj = re.search( "Aggregate Sent .*[-\s]*([^-]*)", entire_file, re.M)
    parse_aggregate_sent(searchObj.group(1))

    power_list = [3600000*power*time_to_hours(send_time) for send_time in send_time_list]
    print ("Power in KWh for each core = %s "%(power_list))

if __name__ == "__main__":
    main(sys.argv)
